using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class FaceController : MonoBehaviour
{
    //[SerializeField]
    private ARFaceManager _arFaceManager;

    [SerializeField]
    private List<FaceComponents> faces;

    private FaceComponents currentFace = new FaceComponents();

    int counter;


    // Start is called before the first frame update
    void Start()
    {
        _arFaceManager = GetComponent<ARFaceManager>();
        SetFace(faces[0]);
    }

    // Update is called once per frame
    void Update()
    {
        foreach (ARFace face in _arFaceManager.trackables)
        {
            currentFace.leftEyeObject.transform.position = face.gameObject.transform.position + face.vertices[230];
            currentFace.rightEyeObject.transform.position = face.gameObject.transform.position + face.vertices[450];
            currentFace.noseObject.transform.position = face.gameObject.transform.position + face.vertices[197];
            currentFace.leftEyeObject.transform.rotation = face.gameObject.transform.rotation;
            currentFace.rightEyeObject.transform.rotation = face.gameObject.transform.rotation;
            currentFace.noseObject.transform.rotation = face.gameObject.transform.rotation;
        }
    }

    private void OnEnable()
    {
        
    }

    public void ShowFaces(bool state)
    {
        foreach (ARFace face in _arFaceManager.trackables)
        {
            face.gameObject.SetActive(state);
        }

        _arFaceManager.enabled = state;
    }

    public void SwitchFaces()
    {
        if(faces.Count > 0)
        {
            counter = counter == faces.Count - 1 ? 0 : counter + 1;
        }
        SetFace(faces[counter]);

    }

    private void SetFace(FaceComponents faceObj)
    {
        Destroy(currentFace.leftEyeObject);
        Destroy(currentFace.rightEyeObject);
        Destroy(currentFace.noseObject);
        Destroy(currentFace);
        foreach (ARFace face in _arFaceManager.trackables)
        {
            if(faceObj.leftEyeObject)
                currentFace.leftEyeObject = Instantiate(faceObj.leftEyeObject, face.gameObject.transform.position + face.vertices[230], Quaternion.identity);
            if (faceObj.rightEyeObject)
                currentFace.rightEyeObject = Instantiate(faceObj.leftEyeObject, face.gameObject.transform.position + face.vertices[450], Quaternion.identity);
            if(faceObj.noseObject)
                currentFace.noseObject = Instantiate(faceObj.leftEyeObject, face.gameObject.transform.position + face.vertices[197], Quaternion.identity);
            face.GetComponent<MeshRenderer>().material = faceObj.faceMaterial;
        }
    }
}
