using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;


[RequireComponent(typeof(ARRaycastManager))]
[RequireComponent(typeof(ARPlaneManager))]
[RequireComponent(typeof(ARAnchorManager))]
[RequireComponent(typeof(ARFaceManager))]
public class InteractionManager : MonoBehaviour
{
    [SerializeField] private GameObject[] _spawnedObjectPrefabs;
    [SerializeField] private GameObject _targetMarkerPrefab;
    [SerializeField] private GameObject[] _uiScreens;
    [SerializeField] private Camera _arCamera;

    //[SerializeField]
    private FaceController _faceController;

    private ARRaycastManager _aRRaycastManager;
    private List<ARRaycastHit> _raycastHits;
    private ARPlaneManager _aRPlaneManager;
    private ARAnchorManager _aRAnchorManager;
    
    
    private GameObject _targetMarker;
    private GameObject _selectedObject;

    //each interaction manager state correlates to UI screens
    private enum InteractionManagerState { Default, SpawnObject, SelectObject, ImageTracker, FaceMask }
    private InteractionManagerState _currentState;
    private UnityAction[] _stateInitializationAction;
    private int _spawnedObjectType = -1;
    private int _spawnedObjectCount = 0;
    private bool _foundPlanes = false;

    private void Awake()
    {
        _aRAnchorManager = GetComponent<ARAnchorManager>();
        _aRAnchorManager.anchorsChanged += OnAnchorsChanged;

        //setup vars
        _aRRaycastManager = GetComponent<ARRaycastManager>();
        _raycastHits = new List<ARRaycastHit>();

        _aRPlaneManager = GetComponent<ARPlaneManager>();
        _aRPlaneManager.planesChanged += OnPlanesChanged;

        _faceController = GetComponent<FaceController>();

        //setup state init action
        //it's array of unity actions called while transition into required state
        //we cast every state to int to place them in array
        _stateInitializationAction = new UnityAction[Enum.GetNames(typeof(InteractionManagerState)).Length];
        _stateInitializationAction[(int)InteractionManagerState.Default] = InitializeDefaultScreen;
        _stateInitializationAction[(int)InteractionManagerState.SpawnObject] = InitializeObjectSpawner;
        _stateInitializationAction[(int)InteractionManagerState.SelectObject] = InitializeObjectSelection;
        _stateInitializationAction[(int)InteractionManagerState.ImageTracker] = InitializeImageTracker;
        _stateInitializationAction[(int)InteractionManagerState.FaceMask] = InitializeFaceMask;
    }

    private void ShowFrontalCamera(bool state)
    {
        ARCameraManager cameraManager = _arCamera.GetComponent<ARCameraManager>();
        if(!cameraManager)
            throw new MissingComponentException(cameraManager.GetType().Name + " component not found!");

        if (state)
        {
            cameraManager.requestedFacingDirection = CameraFacingDirection.User;
        }
        else
        {
            cameraManager.requestedFacingDirection = CameraFacingDirection.World;
        }
    }

    private void InitializeFaceMask()
    {
        _aRRaycastManager.enabled = false;
        _aRAnchorManager.enabled = false;
        _aRPlaneManager.enabled = false;
        ShowPlanes(false);
        ShowFrontalCamera(true);
        _faceController.ShowFaces(true);
    }

    private void InitializeImageTracker()
    {
        ImageTracker tracker = _uiScreens[(int)InteractionManagerState.ImageTracker].GetComponent<ImageTracker>();

        if(!tracker)
            throw new MissingComponentException(tracker.GetType().Name + " component not found!");
        ShowPlanes(false);
        tracker.Initialize();
    }

    private void ShowPlanes(bool state)
    {
        foreach(ARPlane plane in _aRPlaneManager.trackables)
        {
            plane.gameObject.SetActive(state);
        }
        _aRPlaneManager.enabled = state;
    }

    private void OnAnchorsChanged(ARAnchorsChangedEventArgs args)
    {
        foreach (ARAnchor anchor in args.added)
        {
            Debug.Log("Added anchor " + anchor.name);
            
        }
        foreach (ARAnchor anchor in args.updated)
        {
            Debug.Log("Updated anchor " + anchor.name);
        }
        foreach (ARAnchor anchor in args.removed)
        {
            Debug.Log("Removed anchor " + anchor.name);
        }
    }

    private void OnPlanesChanged(ARPlanesChangedEventArgs args)
    {
        foreach(ARPlane plane in args.added)
        {
            Debug.Log("Added plane " + plane.name);
            if (!_foundPlanes)
            {
                _foundPlanes = true;
                UpdateUIScreens();
            }
        }
        foreach (ARPlane plane in args.updated)
        {
            Debug.Log("Updated plane " + plane.name);
        }
        foreach (ARPlane plane in args.removed)
        {
            Debug.Log("Removed plane " + plane.name);
        }
    }

    void Start()
    {
        //create target marker
        _targetMarker = Instantiate(
            original: _targetMarkerPrefab,
            position: Vector3.zero,
            rotation: _targetMarkerPrefab.transform.rotation);
        _targetMarker.SetActive(false);

        //reset current state
        _currentState = InteractionManagerState.Default;
        UpdateUIScreens();

        _uiScreens[(int)InteractionManagerState.Default].SetActive(false);
    }


    private void UpdateUIScreens()
    {
        //hide every ui screen
        foreach (GameObject uiObject in _uiScreens)
            uiObject.SetActive(false);

        _uiScreens[(int)_currentState].SetActive(true);

        _stateInitializationAction[(int)_currentState]();
    }

    public void DisplayUIScreen(int screenNumber)
    {
        _currentState = (InteractionManagerState)screenNumber;
        UpdateUIScreens();
    }

    public void DisplayDefaultScreen()
    {
        _currentState = InteractionManagerState.Default;
        _targetMarker.SetActive(false);
        ShowPlanes(true);
        _faceController.ShowFaces(false);
        UpdateUIScreens();
    }

    public void SelectSpawnedObjectType(int objectType)
    {
        _spawnedObjectType = objectType;
    }

    private void InitializeDefaultScreen()
    {
        Debug.Log("Initialize default screen");
        _selectedObject = null;
        _aRPlaneManager.enabled = true;
        _aRRaycastManager.enabled = true;
        _aRAnchorManager.enabled = true;
        ShowMarker(false);
    }

    private void InitializeObjectSpawner()
    {
        Debug.Log("Initialize spawner");
        _spawnedObjectType = -1;
    }

    void Update()
    {
        if(Input.touchCount > 0)
        {
            if (!_foundPlanes)
                return;

            Touch touch1 = Input.GetTouch(0);
            bool isOverUI = touch1.position.IsPointOverUIObject();

            switch(_currentState)
            {
                case InteractionManagerState.SpawnObject:
                    ProcessTouchSpawnObject(touch1, isOverUI);
                    break;
                case InteractionManagerState.SelectObject:
                    //if one touch then select
                    if(Input.touchCount == 1)
                    {
                        //try to select object, if wasn't possible then move it
                        if(!ProcessTouchSelectObject(touch1, isOverUI))
                        {
                            MoveSelectedObject(touch1);
                        }
                    }
                    else if(Input.touchCount == 2)
                    {
                        RotateSelectedObject(touch1, Input.GetTouch(1));
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private void RotateSelectedObject(Touch touch1, Touch touch2)
    {
        if (!_selectedObject)
            return;

        if(touch1.phase == TouchPhase.Moved || touch2.phase == TouchPhase.Moved)
        {
            float distance = Vector2.Distance(touch1.position, touch2.position);
            float distancePrev = Vector2.Distance(touch1.position - touch1.deltaPosition, touch2.position - touch2.deltaPosition);
            float delta = distance - distancePrev;

            if(Mathf.Abs(delta) > 0.0f)
            {
                delta *= 0.1f;
            }
            else
            {
                delta *= -0.1f;
            }
            _selectedObject.transform.rotation *= Quaternion.Euler(0.0f, delta, 0.0f);
        }
    }

    private void MoveSelectedObject(Touch touch1)
    {
        if (!_selectedObject)
            return;

        if(touch1.phase == TouchPhase.Moved)
        {
            _aRRaycastManager.Raycast(screenPoint: touch1.position,
            hitResults: _raycastHits,
            trackableTypes: TrackableType.Planes);

            _selectedObject.transform.position = _raycastHits[0].pose.position;
        }
    }

    private bool ProcessTouchSelectObject(Touch touch, bool isOverUI)
    {
        bool result = false;
        if(touch.phase == TouchPhase.Began)
        {
            if(!isOverUI)
            {
                result = TrySelectObject(touch.position);
            }
        }
        return result;
    }

    private bool TrySelectObject(Vector2 position)
    {
        Ray ray = _arCamera.ScreenPointToRay(position);
        RaycastHit hitObject;

        if(Physics.Raycast(ray, out hitObject))
        {
            if (hitObject.collider.CompareTag("Spawned Object"))
            {
                _selectedObject = hitObject.collider.gameObject;
                SpawnedObject objectDescription = _selectedObject.GetComponent<SpawnedObject>();
                if (!objectDescription)
                    throw new MissingComponentException(objectDescription.GetType().Name + " component now found!");

                SpawnedObjectDescriptionScreen descriptionScreen = _uiScreens[(int)InteractionManagerState.SelectObject].GetComponent<SpawnedObjectDescriptionScreen>();
                if(!descriptionScreen)
                    throw new MissingComponentException(descriptionScreen.GetType().Name + " component not found!");

                descriptionScreen.ShowObjectDescription(objectDescription);
                return true;
            }
        }
        return false;
    }

    private void ShowMarker(bool value)
    {
        _targetMarker.SetActive(value);
    }

    private void MoveMarker(Vector2 touchPosition)
    {
        _aRRaycastManager.Raycast(
            screenPoint: touchPosition,
            hitResults: _raycastHits,
            trackableTypes: TrackableType.Planes);
        _targetMarker.transform.position = _raycastHits[0].pose.position;
    }

    private void ProcessTouchSpawnObject(Touch touch, bool overUI)
    {
        if (_spawnedObjectType == -1)
            return;

        if(touch.phase == TouchPhase.Began)
        {
            if(!overUI)
            {
                ShowMarker(true);
                MoveMarker(touch.position);
            }
        }
        else if(touch.phase == TouchPhase.Moved)
        {
            MoveMarker(touch.position);
        }
        else if(touch.phase == TouchPhase.Ended)
        {
            SpawnObject(touch);
            ShowMarker(false);
        }
    }

    private void SpawnObject(Touch touch)
    {
        _aRRaycastManager.Raycast(screenPoint: touch.position, 
            hitResults: _raycastHits, 
            trackableTypes: TrackableType.Planes);
        GameObject newObject = Instantiate(
            original: _spawnedObjectPrefabs[_spawnedObjectType], 
            position: _raycastHits[0].pose.position, 
            rotation: _spawnedObjectPrefabs[_spawnedObjectType].transform.rotation
            );

        SpawnedObject spawnedObject = newObject.GetComponent<SpawnedObject>();
        if(!spawnedObject)
            throw new MissingComponentException(spawnedObject.GetType().Name + " component not found!");
        spawnedObject.GiveNumber(++_spawnedObjectCount);

        AnchorObject(newObject);
    }

    private void AnchorObject(GameObject obj)
    {
        if(obj.GetComponent<ARAnchor>() == null)
        {
            obj.AddComponent<ARAnchor>();
        }
    }

    private void InitializeObjectSelection()
    {
        SpawnedObjectDescriptionScreen descScreen = _uiScreens[(int)InteractionManagerState.SelectObject].GetComponent<SpawnedObjectDescriptionScreen>();
        if (!descScreen)
            throw new MissingComponentException(descScreen.GetType().Name + " component not found!");
        descScreen.InitializeScreen();
    }

    public void ProcessFingerTap(int tapCount)
    {
        if (tapCount == 2)
            DisplayDefaultScreen();
    }
}
