using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SpawnedObjectDescriptionScreen : MonoBehaviour
{
    [SerializeField] private GameObject _descriptionPanel;

    [SerializeField] private TMP_Text _objectNameText;
    [SerializeField] private TMP_Text _objectDescriptionText;

    public void InitializeScreen()
    {
        _descriptionPanel.SetActive(false);
    }

    public void ShowObjectDescription(SpawnedObject obj)
    {
        _objectNameText.text = obj.Name;
        _objectDescriptionText.text = obj.Descroption;
        _descriptionPanel.SetActive(true);
    }

    public void BackToMain()
    {
        InteractionManager manager = FindObjectOfType<InteractionManager>();

        if (!manager)
            throw new MissingComponentException(manager.GetType().Name + " now found!");
        manager.DisplayDefaultScreen();
    }
}
