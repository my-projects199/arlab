using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceComponents : MonoBehaviour
{
    public Material faceMaterial;
    public GameObject leftEyeObject;
    public GameObject rightEyeObject;
    public GameObject noseObject;
}
